"""
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
"""

# %%
import subprocess
import sys
import os
import traceback
import platform
import requests
from io import BytesIO
from zipfile import ZipFile
from time import time
from datetime import timedelta
from typing import Any, Callable, List, Union

# %%
system: str = platform.system().lower()
sep: str = os.sep

# %%

wireshark_git_url: str = "https://github.com/wireshark/wireshark.git"

lib_project_name: str = "lua_c_lib"
lib_project_major_version: int = 0
lib_project_minor_version: int = 1
lib_project_patch_version: int = 0

wireshark_major_version: int = 3
wireshark_minor_version: int = 6
wireshark_patch_version: int = 3

builds: List[str] = ["Release"]
debug_enabled: bool = True

if debug_enabled:
    builds.append("Debug")

build_enabled: bool = True

output_prefix: str = f"""{os.getcwd()}"""

wireshark_base_directory: str = f"""{output_prefix}{sep}wireshark"""
wireshark_build_directory: str = f"""{wireshark_base_directory}{sep}build"""
wireshark_binary_base_directory: str = f"""{wireshark_build_directory}{sep}run"""
wireshark_libs_directory: str = f"""{output_prefix}{sep}wireshark-win64-libs-{wireshark_major_version}.{wireshark_minor_version}"""

lib_project_src_directory: str = f"""{os.getcwd()}{sep}src"""
lib_project_build_directory: str = f"""{wireshark_build_directory}{sep}{lib_project_name}"""
lib_project_binary_base_directory: str = f"""{wireshark_binary_base_directory}"""

win_flex_bison_url: str = "https://github.com/lexxmark/winflexbison/releases/download/v2.5.25/win_flex_bison-2.5.25.zip"
strawberry_perl_url: str = "https://strawberryperl.com/download/5.32.1.1/strawberry-perl-5.32.1.1-64bit.zip"

# %%


def get_msbuild_path():
    path: str = ""
    if system == "windows":
        command: str = f"""\"%ProgramFiles(x86)%\\Microsoft Visual Studio\\Installer\\vswhere.exe" -latest -requires Microsoft.Component.MSBuild -find MSBuild\\**\\Bin\\MSBuild.exe"""
        print(f"""call '{command}'""")
        raw_path: bytes = subprocess.check_output(command, shell=True)
        path: str = raw_path.decode("UTF-8")
        path = path.replace("\n", "").replace("\r", "")
        if " " in path:
            path = f"""\"{path}\""""

    return path

# %%


def clone_git(git_url: str, target_directory: str, version: Union[str, None]):
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Get {git_url} source""")
    try:
        os.makedirs(target_directory, exist_ok=True)
        command: str = f"""git clone {git_url} {target_directory}"""
        print(f"""call '{command}'""")
        os.system(command)
        if version is not None:
            os.chdir(target_directory)
            os.system(f"""git fetch --all --tags""")
            os.system(f"""git checkout tags/{version}""")
            os.system(f"""git submodule update --init --recursive""")
    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Abort? (y/n)")
        if abort.lower() == "y":
            sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%
def get_wireshark_dependencies():
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Get wireshark dependencies""")
    if system == "windows":
        try:
            os.environ["PLATFORM"] = "Win64"
            os.environ["WIRESHARK_BASE_DIR"] = wireshark_base_directory
            os.environ["WIRESHARK_LIB_DIR"] = wireshark_libs_directory

            # win_flex_bison
            if not os.path.exists(f"""{wireshark_libs_directory}{sep}win_flex_bison"""):
                print(f"""### Get win flex bison""")
                with requests.get(win_flex_bison_url) as win_flex_bison_request_response:
                    with ZipFile(BytesIO(win_flex_bison_request_response.content)) as win_flex_bison_zip_file:
                        win_flex_bison_zip_file.extractall(
                            f"""{wireshark_libs_directory}{sep}win_flex_bison""")

            os.environ["PATH"] += f""";{wireshark_libs_directory}{sep}win_flex_bison"""

            # strawberry_perl
            if not os.path.exists(f"""{wireshark_libs_directory}{sep}strawberry_perl"""):
                print(f"""### Get strawberry perl""")
                with requests.get(strawberry_perl_url) as strawberry_perl_request_response:
                    with ZipFile(BytesIO(strawberry_perl_request_response.content)) as strawberry_perl_zip_file:
                        strawberry_perl_zip_file.extractall(
                            f"""{wireshark_libs_directory}{sep}strawberry_perl""")

            os.environ["PATH"] += f""";{wireshark_libs_directory}{sep}strawberry_perl{sep}perl{sep}bin"""

        except Exception:
            print(f"""### Error: {traceback.format_exc()}""")
            abort: str = input(f"Abort? (y/n)")
            if abort.lower() == "y":
                sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")

# %%


def integrate_into_wireshark():
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Integrate into Wireshark""")
    try:
        cmake_file_content: str = ""
        cmake_file_content += f"""
            # BEGIN LIB_PROJECT
            set(LIB_PROJECT_NAME {lib_project_name})
            set(LIB_PROJECT_MAJOR_VERSION {lib_project_major_version})
            set(LIB_PROJECT_MINOR_VERSION {lib_project_minor_version})
            set(LIB_PROJECT_PATCH_VERSION {lib_project_patch_version})
            set(LIB_PROJECT_SRC_DIRECTORY {lib_project_src_directory})
            set(LIB_PROJECT_BUILD_DIRECTORY {lib_project_build_directory})
            set(LIB_PROJECT_BINARY_BASE_DIRECTORY {lib_project_binary_base_directory})
            """

        if system == "windows":
            cmake_file_content += f"""
                set(ENV{{PLATFORM}} "Win64")
                set(ENV{{WIRESHARK_BASE_DIR}} {wireshark_base_directory})
                set(ENV{{WIRESHARK_LIB_DIR}} {wireshark_libs_directory})
                """

            if "QT5_BASE_DIR" in os.environ and os.environ["QT5_BASE_DIR"] != "":
                cmake_file_content += f"""
                    set(BUILD_wireshark ON)
                    set(ENV{{QT5_BASE_DIR}} {os.environ["QT5_BASE_DIR"]})
                    """
            else:
                cmake_file_content += f"""set(BUILD_wireshark OFF)\n"""

            cmake_file_content = cmake_file_content.replace("\\", "/")

        cmake_file_content = cmake_file_content.replace("    ", "")
        cmake_file_content += f"""# END LIB_PROJECT\n"""

        if os.path.exists(f"""{wireshark_base_directory}{sep}CMakeLists.backup"""):
            with open(f"""{wireshark_base_directory}{sep}CMakeLists.backup""", "r", encoding="UTF-8") as cmake_file:
                cmake_file_content += cmake_file.read()
                cmake_file.close()
        else:
            with open(f"""{wireshark_base_directory}{sep}CMakeLists.txt""", "r", encoding="UTF-8") as cmake_file:
                cmake_file_content_backup: str = cmake_file.read()
                cmake_file.close()

            with open(f"""{wireshark_base_directory}{sep}CMakeLists.backup""", "w", encoding="UTF-8") as cmake_file:
                cmake_file.write(cmake_file_content_backup)
                cmake_file.flush()
                cmake_file.close()

            cmake_file_content += cmake_file_content_backup

        cmake_file_content += """
            # BEGIN LIB_PROJECT
            add_subdirectory(${LIB_PROJECT_SRC_DIRECTORY} ${LIB_PROJECT_BUILD_DIRECTORY})
            if(BUILD_wireshark)
                add_dependencies(wireshark ${LIB_PROJECT_NAME})
            endif()
            add_dependencies(tshark ${LIB_PROJECT_NAME})
            # END LIB_PROJECT
            """.replace("    ", "")

        with open(f"""{wireshark_base_directory}{sep}CMakeLists.txt""", "w", encoding="UTF-8") as cmake_file:
            cmake_file.write(cmake_file_content)
            cmake_file.flush()
            cmake_file.close()

    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Abort? (y/n)")
        if abort.lower() == "y":
            sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")

# %%


def generate_wireshark():
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Generate wireshark""")
    try:
        os.makedirs(wireshark_build_directory, exist_ok=True)
        os.chdir(wireshark_build_directory)

        cmake_command: str = f"""cmake {wireshark_base_directory} """
        if system == "windows":
            cmake_command += """ -G "Visual Studio 16 2019" -A x64"""
            cmake_command = cmake_command.replace("\\", "/")

        print(f"""call '{cmake_command}'""")
        os.system(cmake_command)

    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Abort? (y/n)")
        if abort.lower() == "y":
            sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")

# %%


def build_wireshark():
    start_time: float = time()
    currend_working_directory: str = os.getcwd()

    print(f"""### Build wireshark""")
    try:
        os.chdir(wireshark_build_directory)
        if system == "windows":
            msbuild_path: str = get_msbuild_path()
            for build in builds:
                command: str = f"""{msbuild_path} /m /p:Configuration={build} /p:Platform=x64 Wireshark.sln"""
                print(f"""call '{command}'""")
                os.system(command)

        elif system == "linux":
            command: str = f"""make -j{os.cpu_count()} CXXFLAGS='-fPIC'"""
            print(f"""call '{command}'""")
            os.system(command)

    except Exception:
        print(f"""### Error: {traceback.format_exc()}""")
        abort: str = input(f"Abort? (y/n)")
        if abort.lower() == "y":
            sys.exit()

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%


def main():
    start_time: float = time()

    currend_working_directory: str = os.getcwd()

    wireshark_tag: str = f"v{wireshark_major_version}.{wireshark_minor_version}.{wireshark_patch_version}"

    clone_git(wireshark_git_url, wireshark_base_directory, wireshark_tag)

    get_wireshark_dependencies()

    integrate_into_wireshark()

    generate_wireshark()

    if build_enabled:
        build_wireshark()
        pass

    os.chdir(currend_working_directory)

    duration: float = time() - start_time
    print(f"""### Done in {timedelta(seconds=duration)} s""")


# %%
if __name__ == "__main__":
    main()

/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#ifndef __LUA_UTIL_H__
#define __LUA_UTIL_H__

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
#ifdef _WIN32

#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"

#else

#include <lua5.2/lauxlib.h>
#include <lua5.2/lua.h>
#include <lua5.2/lualib.h>

#endif // _WIN32

#ifdef __cplusplus
}
#endif // __cplusplus

// define platform specific EXPORT_FUNCTION macro
#ifdef _WIN32
#define EXPORT_FUNCTION extern "C" __declspec(dllexport)
#else
#define EXPORT_FUNCTION extern "C"
#endif // _WIN32

#define __CONCAT(A, B) A##B
#define CONCAT(A, B) __CONCAT(A, B)
#define luaopen_lib CONCAT(luaopen_, LIB_PROJECT_NAME)

int pretty_format_stack(lua_State *L);

#endif // __LUA_UTIL_H__
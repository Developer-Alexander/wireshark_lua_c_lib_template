/*
Copyright 2022 Developer Alexander <dev at alex-mails.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR
A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

SPDX-License-Identifier: MIT
*/

#include "lua_util.h"

#include <sstream>
#include <string>
using namespace std;

int pretty_format_stack(lua_State *L)
{
    int stack_count = lua_gettop(L);

    ostringstream output_stream;

    for (int index = stack_count; index >= 1; index--)
    {
        int relative_index = -stack_count + index - 1;

        output_stream << index << " (" << relative_index << "): " << lua_typename(L, lua_type(L, index)) << "\n";
    }

    string result = output_stream.str();
    lua_pushlstring(L, result.c_str(), result.size());

    return 1;
}

static int get_major_version(lua_State *L)
{
    lua_pushinteger(L, LIB_PROJECT_MAJOR_VERSION);
    return 1;
}

static int get_minor_version(lua_State *L)
{
    lua_pushinteger(L, LIB_PROJECT_MINOR_VERSION);
    return 1;
}

static int get_patch_version(lua_State *L)
{
    lua_pushinteger(L, LIB_PROJECT_PATCH_VERSION);
    return 1;
}

// luaopen_lib will be exported after being expanded to luaopen_${PROJECT_NAME}
EXPORT_FUNCTION int luaopen_lib(lua_State *L)
{
    static const struct luaL_Reg __lib[] = {
        // Add all functions of your lib to this list
        {"pretty_format_stack", pretty_format_stack},
        {"get_major_version", get_major_version},
        {"get_minor_version", get_minor_version},
        {"get_patch_version", get_patch_version},
        {NULL, NULL} // Sentinel
    };

    luaL_newlib(L, __lib);
    return 1;
}
This is a template for building a lua library for wireshark written in C.

While a lua C library could be built standalone this project setup becomes useful when debugging of the library is needed.

# Build

Lua C library integrates itself into Wireshark CMake project.

## Windows

### Requirements

1. git
2. CMake
3. Visual Studio 2019 (As of Feb. 2022 Wireshark requires Visual Studio 2019)
4. Python 3

For `Wireshark.exe` you need additionally (otherwise you will get only command line version `tshark.exe`):

5. Qt 5 (most recent LTS release is recommended)

### Steps

1. If you want to build with Qt for `Wireshark.exe` set environment variable `QT5_BASE_DIR` to the path of your Qt installation

   - e.g. `QT5_BASE_DIR=D:\Dev\wireshark_dependencies\qt_5.15\msvc2019_64`

2. Run `build.py` in the root directory

   - The CMake root for Wireshark will be `wireshark/`.

   - The Visual Studio Solution for Wireshark can be found in `wireshark/build/Wireshark.sln`. (Except you use a custom CMake output path)

3. Resulting binaries can be found in `wireshark/build/run/<RELEASE_NAME>/`

## Linux

### Requirements

Required packages:

`sudo add-apt-repository universe`

`sudo apt-get update`

`sudo apt-get install build-essential git cmake python3 gdb`

Wireshark dependencies:

`sudo apt-get install flex bison perl libglib2.0-dev libc-ares-dev libpcap-dev qttools5-dev qttools5-dev-tools libqt5svg5-dev qtmultimedia5-dev libgcrypt20-dev liblua5.2-dev libnghttp2-dev libpcre2-dev`

Note: Please check out documentation of Wireshark for information about optional packages.

### Steps

1. Run `build.py` in the root directory

   - The CMake root for Wireshark will be `wireshark/`.

   - The Makefile project for Wireshark can be found in `wireshark/build/`. (Except you use a custom CMake output path)

2. Resulting binaries can be found in `wireshark/build/run/`

# License

Source code of this template licensed under MIT license.

SPDX identifier: MIT
